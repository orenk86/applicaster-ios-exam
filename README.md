**Oren Kosto's Exam app for Applicaster**

This app uses libraries installed via Cocoapods.
To run the app, download or clone the repository, run:

	pod install

And open ***Applicaster Exam.xcworkspace*** using Xcode.
