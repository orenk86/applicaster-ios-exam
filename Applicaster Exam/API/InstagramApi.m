//
//  InstagramApi.m
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "InstagramApi.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "AppUtils.h"

//https://api.instagram.com/v1/media/search?lat=48.858844&lng=2.294351&access_token=821bc4458537c2064382fd0165ec5a57c29c39c36b9195bdb5a72673950baab7

@interface InstagramApi()

@property (strong, nonatomic) NSURLSessionConfiguration *configuration;
@property (strong, nonatomic) AFURLSessionManager *manager;

@end

@implementation InstagramApi

static InstagramApi *sharedInstance = nil;
static dispatch_once_t onceToken;
static BOOL isSetup = NO;

+(InstagramApi *) sharedInstance
{
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void)setup {
    self.configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:self.configuration];
}

-(void)requestPostsForLocation:(CLLocation *)location withRadius:(NSString * _Nullable)radius success:(void (^)(NSArray <NSDictionary *> * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure
{
    NSString *lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    NSString *lng = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"lat": lat,
                                                                                  @"lng": lng,
                                                                                  @"radius": [AppUtils getLocalConfiguration:CONFIG_KEY_INSTAGRAM_RADIUS],
                                                                                  @"access_token": [AppUtils getLocalConfiguration:INSTAGRAM_API_KEY]
                                                                                  }];
    if (radius) {
        params[@"radius"] = radius;
    }
    
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                          URLString:INSTAGRAM_SEARCH_REQUEST_URL
                                                                         parameters:params
                                                                              error:nil];
    NSURLSessionDataTask *task = [self.manager dataTaskWithRequest:request
                                                 completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary * _Nullable responseObject, NSError * _Nullable error) {
                                                     if (error
                                                         || !responseObject[@"data"]
                                                         || ![responseObject[@"data"] isKindOfClass:[NSArray class]]) {
                                                         failure(error);
                                                     } else {
                                                         NSArray *results = responseObject[@"data"];
                                                         success(results);
                                                     }
                                                 }];
    
    NSLog(@"Requesting posts for location: %@, %@ and radius: %@", lat, lng, radius);
    [task resume];
}

@end
