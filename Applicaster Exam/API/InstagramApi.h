//
//  InstagramApi.h
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface InstagramApi : NSObject

+(InstagramApi * _Nonnull) sharedInstance;

-(void)requestPostsForLocation:(CLLocation * _Nonnull)location
                    withRadius:(NSString * _Nullable)radius
                       success:(nullable void (^)(NSArray <NSDictionary *> * _Nonnull))success
                       failure:(nullable void (^)(NSError * _Nullable))failure;

@end
