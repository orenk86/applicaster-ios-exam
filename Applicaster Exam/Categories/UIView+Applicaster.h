//
//  UIView+Applicaster.h
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FADE_DURATION 0.5

@interface UIView (Applicaster)

/**
 *  @name Public Methods
 */

/**
 *  A convenience function that makes the view fade in.
 */
- (void)fadeIn;

/**
 *  A convenience function that makes the view fade out.
 */
- (void)fadeOut;

/**
 *  A convenience function that makes the view fade in, with a custom fade duration.
 *
 *  @param duration The duration of the fade animation.
 */
- (void)fadeInWithDuration:(CGFloat)duration;

/**
 *  A convenience function that makes the view fade out, with a custom fade duration.
 *
 *  @param duration The duration of the fade animation.
 */
- (void)fadeOutWithDuration:(CGFloat)duration;

/**
 *  A convenience function that makes the view fade out, with a custom target alpha.
 *
 *  @param targetAlpha The alpha to set to the view in the fade animation.
 */
- (void)fadeInWithTargetAplha:(CGFloat)targetAlpha;

/**
 *  A convenience function that makes the view fade out, with a custom fade duration and target alpha.
 *
 *  @param duration    duration The duration of the fade animation.
 *  @param targetAlpha The alpha to set to the view in the fade animation.
 */
- (void)fadeInWithDuration:(CGFloat)duration withTargetAplha:(CGFloat)targetAlpha;

/**
 * Gets this view's containing view controller.
 *
 * @return This view's containing view controller.
 */
- (UIViewController *)viewController;

@end
