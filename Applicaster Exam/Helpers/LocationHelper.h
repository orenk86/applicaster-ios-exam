//
//  LocationHelper.h
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationHelper : NSObject

@property (strong, nonatomic, readonly) NSArray<NSString *> *radiusOptions;
@property (assign, nonatomic, readonly) int currentRadiusndex;
@property (strong, nonatomic, readonly) NSString *radius;

@property (strong, nonatomic, readonly) CLLocation *currentLocation;
@property (strong, nonatomic, readonly) NSString *currentLocationName;

@property (strong, nonatomic, readonly) NSArray <NSDictionary *> *posts;

+(LocationHelper *) sharedInstance;
-(BOOL)hasPermission;
-(void)requestPermission;
-(void)start;
-(void)stop;

-(void)setRadiusIndex:(int)radiusIndex;
-(void)setRadius:(NSString *)radius;
-(void)expandRadius;

@end
