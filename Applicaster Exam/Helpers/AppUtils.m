//
//  AppUtils.m
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "AppUtils.h"

@implementation AppUtils

static AppUtils *sharedUtils = nil;
static dispatch_once_t onceToken;
static NSMutableDictionary *plistDictionary;
static NSDictionary *defaultLocalConfig;
static BOOL isSetup = NO;

+(AppUtils *) sharedUtils
{
    dispatch_once(&onceToken, ^{
        sharedUtils = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedUtils setup];
        isSetup = YES;
    }
    return sharedUtils;
}

-(void)setup {
    [self initLocalConfig];
}

- (void) initLocalConfig {
    // Load configuration from settings.plist if exists
    NSString *pathToPlist = [[NSBundle mainBundle] pathForResource:@"PVPSettings" ofType:@"plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToPlist]) {
        NSLog(@"Local configuration file settings.plist found in the main app bundle.");
        plistDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pathToPlist];
    } else {
        plistDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    
    defaultLocalConfig = @{CONFIG_KEY_INSTAGRAM_RADIUS: @"500",
                           CONFIG_KEY_MONITOR_REACHABILITY: @(YES),
                           DEFAULTS_KEY_HAS_VISITED_APP: @([[AppUtils getDefaultValueFofKey:DEFAULTS_KEY_HAS_VISITED_APP] boolValue])
                           };
}

+ (NSString *)getLocalConfiguration:(NSString *)name {
    if (!plistDictionary) {
        [[self sharedUtils] initLocalConfig];
    }
    return plistDictionary[name] ? plistDictionary[name] : defaultLocalConfig[name];
}

+ (void)setLocalConfiguration:(NSString *)configuration forKey:(NSString *)key {
    if (!plistDictionary) {
        [[self sharedUtils] initLocalConfig];
    }
    plistDictionary[key] = configuration;
}

+(void) saveDefaultValue:(NSString *)value forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+(NSString *) getDefaultValueFofKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:key];
}

+(void) performSelector:(SEL)selector on:(id)delegate {
    if ([delegate respondsToSelector:selector]) {
        IMP imp = [delegate methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(delegate, selector);
    } else {
        NSLog(@"Method: %@ missing for delegate: %@", NSStringFromSelector(selector), NSStringFromClass([delegate class]));
    }
}

+(void) performSelector:(SEL)selector on:(id)delegate withObject:(id)object {
    if ([delegate respondsToSelector:selector]) {
        IMP imp = [delegate methodForSelector:selector];
        void (*func)(id, SEL, id) = (void *)imp;
        func(delegate, selector, object);
    } else {
        NSLog(@"Method: %@ missing for delegate: %@", NSStringFromSelector(selector), NSStringFromClass([delegate class]));
    }
}

+ (BOOL)hasVisitedApp {
    return [[self getLocalConfiguration:DEFAULTS_KEY_HAS_VISITED_APP] boolValue];
}

@end
