//
//  LocationHelper.m
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "LocationHelper.h"
#import "InstagramApi.h"
#import "AlertHelper.h"

@interface LocationHelper () <CLLocationManagerDelegate>

@property(nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation LocationHelper

static LocationHelper *sharedInstance = nil;
static dispatch_once_t onceToken;
static BOOL isSetup = NO;

+(LocationHelper *) sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void)setup {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _radiusOptions = @[@"500", @"1000", @"2500", @"5000"];
    _currentRadiusndex = 0;
    _radius = self.radiusOptions[_currentRadiusndex];
}

-(void)setRadius:(NSString *)radius {
    for (int i = 0; i < _radiusOptions.count; i++) {
        if ([radius isEqualToString:_radiusOptions[i]]) {
            [self setRadiusIndex:i];
            break;
        }
    }
}

-(void)setRadiusIndex:(int)radiusIndex {
    if (radiusIndex >= 0 && radiusIndex < _radiusOptions.count) {
        _currentRadiusndex = radiusIndex;
        _radius = self.radiusOptions[_currentRadiusndex];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_RADIUS_CHANGE object:nil userInfo:@{}];
        
        [self getDataFromInstagram];
    }
}

-(void)expandRadius {
    [self setRadiusIndex:_currentRadiusndex + 1];
}

#pragma mark permission handling

/*
 * Checks if the location permission has been properly configured in the Info.plist and authorized.
 */
-(BOOL)hasPermission {
    if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
        return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways;
    } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
        return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse;
    } else {
        // no permission message in the info.plist. throw an exception?
        NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        return NO;
    }
}

/*
 * Checks if the location permission was denied, or turned off manually in the settings app.
 */
-(BOOL)permissionDenied {
    return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted
    || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied;
}

/*
 * Display a dialog to redirect the user to the settings app in case the location permission is denied or blocked manually.
 */
-(void)handlePermissionDenied {
    [AlertHelper showAlertWithTitle:TEXT_PERMISSION_RESTRICTED_TITLE
                         andMessage:TEXT_PERMISSION_RESTRICTED_MESSAGE
              withCancelButtonTitle:TEXT_MAYBE_LATER
               withOtherButtonTitle:TEXT_OPEN_SETTINGS
                        withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
                            if (confirmed) {
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                            }
                        }];
}

-(void)requestPermission {
    if ([self permissionDenied]) {
        [self handlePermissionDenied];
    } else if (![self hasPermission] && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
        // in case we switch between permission types
        if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            [self.locationManager requestAlwaysAuthorization];
        } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            [self.locationManager  requestWhenInUseAuthorization];
        } else {
            // no permission message in the info.plist. throw an exception?
        }
    }
}

-(void)start {
    [self requestPermission];
    [self.locationManager startUpdatingLocation];
}

-(void)stop {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark CLLocationManagerDelegate methods
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_PERMISSION_STATE_CHANGE object:nil userInfo:@{}];
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self start];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (locations.count > 0) {
        [self stop];
        _currentLocation = locations[0];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_CHANGE object:nil userInfo:@{}];
        NSLog(@"Got location: %f, %f", _currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
        
        [self getDataFromInstagram];
    }
}

#pragma mark Instagram API
-(void)getDataFromInstagram {
    [[InstagramApi sharedInstance] requestPostsForLocation:_currentLocation
                                                withRadius:_radius
                                                   success:^(NSArray<NSDictionary *> *result) {
                                                       _posts = result;
                                                       [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_POSTS_CHANGE object:nil userInfo:@{}];
                                                       if (result.count == 0 && _currentRadiusndex < _radiusOptions.count) {
                                                           [self expandRadius];
                                                       }
                                                   } failure:^(NSError *e) {}];
}

@end
