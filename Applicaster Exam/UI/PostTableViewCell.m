//
//  PostTableViewCell.m
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/9/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "PostTableViewCell.h"
#import "NSDictionary+Applicaster.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PostTableViewCell()

@property(nonatomic, strong) NSDictionary *post;

@end

@implementation PostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPost:(NSDictionary *)post {
    _post = post;
    self.userNameLabel.text = [post safeValueForKeyPath:@"user.username" withDefaultValue:@""];
    self.postLabel.text = [post safeValueForKeyPath:@"caption.text" withDefaultValue:@""];
    
    NSString *userImageUrl = [post safeValueForKeyPath:@"user.profile_picture" withDefaultValue:nil];
    if (userImageUrl) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:userImageUrl]];
    } else {
        [self.userImage setImage:nil];
    }
    
    NSString *postImageUrl = [post safeValueForKeyPath:@"images.standard_resolution.url" withDefaultValue:nil];
    if (postImageUrl) {
        [self.postImage sd_setImageWithURL:[NSURL URLWithString:postImageUrl]];
    } else {
        [self.postImage setImage:nil];
    }
}

@end
