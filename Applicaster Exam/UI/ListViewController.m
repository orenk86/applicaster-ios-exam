//
//  ListViewController.m
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/9/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "ListViewController.h"
#import "PostTableViewCell.h"
#import "LocationHelper.h"
#import "NSDictionary+Applicaster.h"
#import "Constants.h"
#import "AlertHelper.h"

#define TABLEVIEW_CELL_REUSE_IDENTIFIER @"PostTableViewCell"

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor clearColor];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    [refresh addTarget:self action:@selector(refreshEntryList) forControlEvents:UIControlEventValueChanged];
    self.tableView.refreshControl = refresh;
}

- (void)refreshEntryList {
    [[LocationHelper sharedInstance] start];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postsHaveChanged:) name:NOTIFICATION_POSTS_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(radiusHasChanged:) name:NOTIFICATION_RADIUS_CHANGE object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)postsHaveChanged:(NSNotification *)notification {
    if ([self.tableView.refreshControl isRefreshing]) {
        [self.tableView.refreshControl endRefreshing];
    }
    [self.tableView reloadData];
}

- (void)radiusHasChanged:(NSNotification *)notification {
    [self.radiusLabel setText:[NSString stringWithFormat:@"%@m", [LocationHelper sharedInstance].radius]];
}

- (IBAction)selectRadius:(UIButton *)sender {
    [AlertHelper showActionSheetWithTitle:@"Radius" andMessage:@"Select Search Radius" withCancelButtonTitle:TEXT_ALERT_CANCEL withDestructiveButtonTitles:nil withOtherButtonTitles:[LocationHelper sharedInstance].radiusOptions withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed, NSString * _Nonnull buttonTitle) {
        if (confirmed) {
            [[LocationHelper sharedInstance] setRadius:buttonTitle];
        }
    }];
}

#pragma mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TABLEVIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    if ([LocationHelper sharedInstance].posts != nil && [LocationHelper sharedInstance].posts.count > indexPath.row) {
        [cell setPost:[LocationHelper sharedInstance].posts[indexPath.row]];
    }
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [LocationHelper sharedInstance].posts == nil ? 0 : [LocationHelper sharedInstance].posts.count;
}

#pragma mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
