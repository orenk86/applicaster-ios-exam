//
//  WelcomeViewController.h
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *introLabel1;
@property (weak, nonatomic) IBOutlet UILabel *introLabel2;
@property (weak, nonatomic) IBOutlet UILabel *introLabel3;
@property (weak, nonatomic) IBOutlet UILabel *introLabel4;

@end
