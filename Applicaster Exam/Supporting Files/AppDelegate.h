//
//  AppDelegate.h
//  Applicaster Exam
//
//  Created by Oren Kosto on 11/12/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

